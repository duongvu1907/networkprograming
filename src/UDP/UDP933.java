/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UDP;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author duong
 */
public class UDP933 {
    public static void main(String[] args) throws UnknownHostException, SocketException, IOException, ClassNotFoundException {
        InetAddress inet = InetAddress.getByName("203.162.88.100");
        int port = 1109;
        DatagramSocket socket = new DatagramSocket();
        
        byte[] sendData;
        byte[] receiveData = new byte[1024];
        
        Student933 student = new Student933("B15DCCN161");
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteOut);
        out.writeObject(student);
        sendData = byteOut.toByteArray();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, inet, port);
        socket.send(sendPacket);
        
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        socket.receive(receivePacket);
        byte[] receiveDataObject = receivePacket.getData();
        ByteArrayInputStream byteIn = new ByteArrayInputStream(receiveDataObject);
        ObjectInputStream in = new ObjectInputStream(byteIn);
        
        Student933 student933 = (Student933) in.readObject();
        String id = student933.getId();
        String code = student933.getCode();
        String name = student933.getName();
        StringBuffer str_name = new StringBuffer();
        
        Matcher match = Pattern.compile("([a-z])([a-z]*)",Pattern.CASE_INSENSITIVE).matcher(name);
        while (match.find()) {            
            match.appendReplacement(str_name,match.group(1).toUpperCase()+match.group(2).toLowerCase());
        }
        name = match.appendTail(str_name).toString();
        
        String[] nameArr = name.split(" ");
        
        StringBuilder email = new StringBuilder();
        email.append(nameArr[nameArr.length-1]);
        
        for (int i = 0; i < nameArr.length-1; i++) {
            email.append(nameArr[i].charAt(0));
        }
        email.append("@ptit.edu.vn");
        String emailResult = email.toString().toLowerCase();
        System.out.println(name+"|"+emailResult);
        
        Student933 st = new Student933(id, code, name, emailResult);
        
        System.out.println(st.getId()+"|"+st.getName()+"|"+st.getCode()+"|"+st.getEmail());
        
        ByteArrayOutputStream byteOut_ = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(byteOut_);
        oos.writeObject(st);
        byte[] sendData_ = byteOut_.toByteArray();
        
        DatagramPacket sendPacketFinal = new DatagramPacket(sendData_, sendData_.length, inet, port);
        socket.send(sendPacketFinal);
        socket.close();
    }
}
