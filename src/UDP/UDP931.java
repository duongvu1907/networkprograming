/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 *
 * @author duong
 */
public class UDP931 {
    public static void main(String[] args) throws UnknownHostException, SocketException, IOException {
        InetAddress inet = InetAddress.getByName("203.162.88.100");
        int port = 1107;
        
        DatagramSocket socket = new DatagramSocket();
        
        byte[] sendData;
        byte[] receiveData = new byte[1024];
        String msv = ";B15DCCN161;931";
        
        sendData = msv.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length, inet, port);
        socket.send(sendPacket);
        
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        socket.receive(receivePacket);
        String str = new String(receivePacket.getData());
        
        String[] strArr = str.split(";");
        String requestId = strArr[0];
        String[] squence = strArr[1].trim().split(",");
        
        int min = Integer.parseInt(squence[0]);
        int max = Integer.parseInt(squence[0]);
        
        for (String squence1 : squence) {
            int m = Integer.parseInt(squence1);
            if (m<min) {
                min=m;
            }
            if (m>max) {
                max=m;
            }
        }
        String result = requestId+";"+max+","+min;
        byte[] sendResult = result.getBytes();
        DatagramPacket sendResultPacket = new DatagramPacket(sendResult, sendResult.length, inet, port);
        socket.send(sendResultPacket);
        socket.close();
    }
}
