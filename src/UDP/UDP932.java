/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author duong
 */
public class UDP932 {
    public static void main(String[] args) throws UnknownHostException, SocketException, IOException {
        InetAddress inet = InetAddress.getByName("203.162.88.100");
        int port = 1108;
        DatagramSocket socket = new DatagramSocket();
        
        byte[] sendData;
        byte[] receiveData = new byte[1024];
        String msv = ";B15DCCN161;932";
        sendData = msv.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, inet, port);
        socket.send(sendPacket);
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        socket.receive(receivePacket);
        
        String str = new String(receivePacket.getData());
        String[] strArr = str.split(";");
        String requestId = strArr[0];
        String data = strArr[1];

        StringBuffer strBuff = new StringBuffer();
        Matcher match = Pattern.compile("([a-z])([a-z]*)",Pattern.CASE_INSENSITIVE).matcher(data);
        while (match.find()) {            
            match.appendReplacement(strBuff,match.group(1).toUpperCase()+match.group(2).toLowerCase());
        }
        String result = requestId+";" + match.appendTail(strBuff).toString();
        
        DatagramPacket sendResulPacket = new DatagramPacket(result.getBytes(), result.getBytes().length, inet, port);
        socket.send(sendResulPacket);
        socket.close();
    }
   
}
