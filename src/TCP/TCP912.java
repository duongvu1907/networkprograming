/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author duong
 */
public class TCP912 {
        public static void main(String[] args) throws UnknownHostException, IOException {
            
        InetAddress inet = InetAddress.getByName("203.162.88.100");
        int port = 1108;
        Socket socket = new Socket(inet,port);
        
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            
            String msv = "B15DCCN161;912";
            out.write(msv);
            out.newLine();
            out.flush();
            
            String str = in.readLine();
            String character = str.replaceAll("\\w", "");
            String word = str.replaceAll("\\W", "");
            
            out.write(word);
            out.newLine();
            out.flush();
            
            out.write(character);
            out.newLine();
            out.flush();
            socket.close();
    }
}
