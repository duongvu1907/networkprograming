/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author duong
 */
public class TCP911 {
    public static void main(String[] args) throws UnknownHostException, IOException {
        InetAddress inet = InetAddress.getByName("203.162.88.100");
        int port = 1107;
        
        Socket socket = new Socket(inet,port);
        
        DataInputStream in = new DataInputStream(socket.getInputStream());
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        
        String msv = "B15DCCN161;911";
        out.writeUTF(msv);
        
        int a = in.readInt();
        int b = in.readInt();
        
        int multi = a*b;
        int sum = a+b;
        int uscln = USCLN(a, b);
        int bcnn = multi/uscln;
        
        out.writeInt(uscln);
        out.writeInt(bcnn);
        out.writeInt(sum);
        out.writeInt(multi);
        
        socket.close();
        
    }
    public static int USCLN(int a,int b){
        if (b==0) {
            return a;
        }
        return USCLN(b,a%b);
    }
}
