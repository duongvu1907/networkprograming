/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author duong
 */
public class TCP913 {
    public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
         InetAddress inet = InetAddress.getByName("203.162.88.100");
        int port = 1109;
        Socket socket = new Socket(inet,port);
        String msv = "B15DCCN161;913";
        
        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
        
        out.writeObject(msv);
        out.flush();
        
        Student student = (Student) in.readObject();
        float gpa = student.getGpa();
        String gpaLetter = checkGpa(gpa);
        
        student.setGpaLetter(gpaLetter);
        out.writeObject(student);
        out.flush();
        
        socket.close();
    }
    public static String checkGpa(float gpa){
        String gpaLetter;
        if (gpa<4 && gpa>3.7) {
            gpaLetter = "A";
        }else if (gpa<3.7 && gpa>3) {
            gpaLetter = "B";
        }else if (gpa<3.0 && gpa >2.0) {
            gpaLetter = "C";
        }else if (gpa < 2.0 && gpa >1.0) {
            gpaLetter = "D";
        }else{
            gpaLetter = "F";
        }
        return gpaLetter;
    }
}
